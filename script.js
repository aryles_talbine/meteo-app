//let url = 'https://api.openweathermap.org/data/2.5/weather?q=paris&appid=a7c985495bb3d7f18ee7a2034c3ca77b&units=metric';

function recupererMetro(city) {
    let url = 'https://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=a7c985495bb3d7f18ee7a2034c3ca77b&units=metric';
    // Créer une requête
    let requete = new XMLHttpRequest(); // Créer un objet
    requete.open('GET', url); // Premier paramètre GET / POST, deuxième paramètr : url
    requete.responseType = 'json'; // Nous attendons du JSON
    requete.send(); // Nous envoyons notre requête

    // Dèss qu'on reçoit une réponse, cette fonction est executée
    requete.onload = function () {
        if (requete.readyState === XMLHttpRequest.DONE) {
            if (requete.status === 200) {
                var reponse = requete.response; // on stock la réponse
                //console.log(reponse);
                document.querySelector("#ville").textContent = city;
                document.querySelector("#temperature_label").innerText = reponse.main.temp;

            }
            else {
                alert('Un problème est intervenu, merci de revenir plus tard.');
            }

        }

    }
}

let city = document.querySelector("#ville").textContent;
// let temp = document.querySelector("#temperature_label");
// let resp = recupererMetro("alger");
// console.log(resp);
// temp.innerText = resp;
// city.textContent = "alger"
recupererMetro(city);

document.querySelector("#changer").addEventListener('click', () => {
    let nameCity = prompt("Donnez moi une ville !");
    recupererMetro(nameCity);
});
//recupererMetro("Paris");
//setInterval(recupererPrix, 500);